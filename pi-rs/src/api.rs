use std::fs;
use std::str;
use serde_json::json;
use crate::db::{Database,Params};
use crate::utils::format::*;
use crate::utils::lwe::*;
use crate::utils::matrices::*;

#[derive(Clone)]
pub struct Shard {
  db: Database,
  params: Params,
}
impl Shard {
  // Expects a JSON array of base64-encoded strings
  pub fn from_json_file(file_path: &str, lwe_dim: usize, m: usize, ele_size: usize, plaintext_bits: usize) -> Self {
    let file_contents: String = fs::read_to_string(file_path).unwrap().parse().unwrap();
    let elements: Vec<String> = serde_json::from_str(&file_contents).unwrap();
    Shard::from_base64_strings(&elements, lwe_dim, m, ele_size, plaintext_bits)
  }

  // Expects an array of base64-encoded strings and converts into a
  // database that can process client queries
  pub fn from_base64_strings(base64_strs: &[String], lwe_dim: usize, m: usize, ele_size: usize, plaintext_bits: usize) -> Self {
    let db = Database::new(base64_strs, m, ele_size, plaintext_bits);
    let params = Params::new(&db, lwe_dim);
    Self { db, params }
  }

  /// Write params and DB to file
  pub fn write_to_file(&self, db_path: &str, params_path: &str) {
    self.db.write_to_file(db_path);
    self.params.write_to_file(params_path);
  }

  // Produces a serialized response (base64-encoded) to a serialized
  // client query
  pub fn respond(&self, serialized_query: &str) -> String {
    let q = Query::deserialize(serialized_query);
    let resp = Response((0..self.db.get_matrix_width_self()).into_iter().map(|i| self.db.vec_mult(&q.to_vec(), i)).collect());
    resp.serialize()
  }

  pub fn get_db(&self) -> Database {
    self.db.clone()
  }

  pub fn get_params(&self) -> Params {
    self.params.clone()
  }
}

/// The `QueryParams` struct is initialized to be used for a client
/// query. This can be done before the client knows which `row_index` it
/// would like to query for.
#[derive(Clone)]
pub struct QueryParams {
  lhs: Vec<u32>,
  rhs: Vec<u32>,
  query: Option<Query>,
  row_index: Option<usize>,
  ele_size: usize,
  plaintext_bits: usize,
}
impl QueryParams {
  pub fn new(params: &mut Params) -> Self {
    // derives the full params if not done already
    params.derive_lhs();
    let s = random_ternary_vector(params.get_dim());
    Self { 
      lhs: params.mult_left(&s), 
      rhs: params.mult_right(&s), 
      query: None, 
      row_index: None, 
      ele_size: params.get_ele_size(), 
      plaintext_bits: params.get_plaintext_bits() 
    }
  }

  /// Prepares a new client query based on an input row_index
  pub fn prepare(&mut self, row_index: usize) {
    let query_indicator = get_rounding_factor(self.plaintext_bits);
    self.lhs[row_index] = self.lhs[row_index] + query_indicator;
    self.row_index = Some(row_index);
    let query = Query(self.lhs.clone());
    self.query = Some(query);
  }

  /// Returns the query object
  pub fn get_query(&self) -> Query {
    self.query.clone().unwrap()
  }
}

/// The `Query` struct holds the necessary information encoded in
/// a client PIR query to the server DB for a particular `row_index`. It
/// provides methods for parsing server responses.
#[derive(Clone)]
pub struct Query(Vec<u32>);
impl Query {
  /// Serializes the query vector into a base64-encoded JSON string
  /// 
  /// TODO: serialize as a byte array rather than JSON
  pub fn serialize(&self) -> String {
    let json = json!(self.0);
    let string = serde_json::to_string(&json).unwrap();
    base64::encode(string)
  }

  /// Deserializes a query from a base64-encoded string
  /// 
  /// TODO: deserialize from byte array rather than JSON
  pub fn deserialize(encoded: &str) -> Self {
    let bytes = base64::decode(encoded).unwrap();
    let json_string = str::from_utf8(&bytes).unwrap();
    Self(serde_json::from_str(&json_string).unwrap())
  }

  pub fn to_vec(&self) -> Vec<u32> {
    self.0.clone()
  }
}

/// The response object wraps a response from a single shard
pub struct Response(Vec<u32>);
impl Response {
  /// Serializes the internal u32 vector as a JSON array and then base64
  /// encodes this.
  ///
  /// TODO: serialize as a byte array rather than JSON
  pub fn serialize(&self) -> String {
    // let bytes = (0..self.db.get_matrix_width_self()).into_iter().map(|i| {
    //   let bytes: [u8; 4] = u32::to_le_bytes(self.db.vec_mult(&q.to_vec(), i));
    //   bytes
    // }).fold(Vec::new(), |mut acc, f| {
    //   acc.extend(f);
    //   acc
    // });
    // base64::encode(bytes)
    let json = json!(self.0);
    let string = serde_json::to_string(&json).unwrap();
    base64::encode(string)
  }

  /// Deserializes the internal u32 vector as a JSON array after base64
  /// decoding.
  ///
  /// TODO: deserialize from byte array rather than JSON
  pub fn deserialize(serialized: &str) -> Self {
    // let row_width = Database::get_matrix_width(ele_size, plaintext_bits);
    // let bytes = base64::decode(serialized).unwrap();
    // let u32_len = std::mem::size_of::<u32>();
    // let mut v = Vec::with_capacity(row_width);
    // for i in 0..row_width {
    //   let buf = bytes[i*u32_len..(i+1)*u32_len].to_vec();
    //   let val = u32::from_le_bytes(u32_sized_bytes_from_vec(buf));
    //   v.push(val);
    // }
    // Self(v)
    let bytes = base64::decode(serialized).unwrap();
    let json_string = str::from_utf8(&bytes).unwrap();
    Self(serde_json::from_str(&json_string).unwrap())
  }

  pub fn to_vec(&self) -> Vec<u32> {
    self.0.clone()
  }

  /// Parses the output as a row of u32 values
  pub fn parse_output_as_row(&self, qp: &QueryParams) -> Vec<u32> {
    // get parameters for rounding
    let rounding_factor = get_rounding_factor(qp.plaintext_bits);
    let rounding_floor = get_rounding_floor(qp.plaintext_bits);
    let plaintext_size = get_plaintext_size(qp.plaintext_bits);

    // perform division and rounding
    (0..Database::get_matrix_width(qp.ele_size, qp.plaintext_bits)).into_iter().map(|i| {
      let unscaled_res = self.0[i].wrapping_sub(qp.rhs[i]);
      let scaled_res = unscaled_res / rounding_factor;
      let scaled_rem = unscaled_res % rounding_factor;
      let mut rounded_res = scaled_res;
      if scaled_rem > rounding_floor {
        rounded_res = rounded_res+1;
      }
      rounded_res % plaintext_size
    }).collect()
  }

  /// Parses the output as a base64-encoded string
  pub fn parse_output_as_base64(&self, qp: &QueryParams) -> String {
    let row = self.parse_output_as_row(qp);
    let remainder = qp.ele_size % qp.plaintext_bits;
    let mut bits = Vec::with_capacity(qp.plaintext_bits*row.len());
    for i in 0..row.len() {
      // We extract either the full amount of bits, or the remainder from
      // the last index
      if i != row.len() - 1 {
        bits.extend(u32_to_bits_le(row[i], qp.plaintext_bits));
      } else {
        bits.extend(u32_to_bits_le(row[i], remainder));
      }
    }
    let bytes = bits_to_bytes_le(&bits);
    base64::encode(bytes)
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use rand_core::{RngCore, OsRng};

  #[test]
  fn client_query_e2e() {
    let m = 2u32.pow(12) as usize;
    let ele_size = 2u32.pow(13) as usize;
    let plaintext_bits = 12 as usize;
    let dim = 512;
    let db_eles = generate_db_eles(m, (ele_size+7)/8);
    let shard = Shard::from_base64_strings(&db_eles, dim, m, ele_size, plaintext_bits);
    let mut params = shard.get_params();
    params.derive_lhs();
    (0..10).into_iter().for_each(|i| {
      let mut qp = QueryParams::new(&mut params);
      qp.prepare(i);
      let q = qp.get_query();
      let d_resp = shard.respond(&q.serialize());
      let resp = Response::deserialize(&d_resp);
      let row_output = resp.parse_output_as_row(&qp);
      let output = resp.parse_output_as_base64(&qp);
      if output != db_eles[i] {
        let mut db = shard.get_db();
        db.switch_fmt();
        panic!("Incorrect response for i={}, expected: {:?}, actual: {:?}", i, db.get_row(i), row_output);
      }
    });
  }

  fn generate_db_eles(num_eles: usize, ele_byte_len: usize) -> Vec<String> {
    let mut eles = Vec::with_capacity(num_eles);
    for _ in 0..num_eles {
      let mut ele = vec![0u8; ele_byte_len];
      OsRng.fill_bytes(&mut ele);
      let ele_str = base64::encode(ele);
      eles.push(ele_str);
    }
    eles
  }
}