use std::sync::mpsc;
use std::thread;
use std::time::Instant;

use bus::Bus;
use rand_core::{RngCore, OsRng};

use pi_rs::api::{QueryParams,Shard,Response};
use pi_rs_cli_utils::*;

const DEBUG: bool = false;
const LOAD_FROM_FILE: bool = false;
const WRITE_TO_FILE: bool = false;
const ELE_BYTE_LEN: usize = 1024;

const QUERY_ROW_INDEX: usize = 10;

fn main() {
  let CLIFlags { m, ele_size, plaintext_bits, lwe_dim, num_shards } = parse_cli_flags();
  let (tx1, rx1) = mpsc::sync_channel(num_shards);
  for i in 0..num_shards {
    let tx1 = tx1.clone();
    thread::spawn(move || {
      let db_start = Instant::now();
      let db_path = String::from(format!("data/db_cached_{}.json", i));
      let params_path = String::from(format!("data/params_cached_{}.json", i));
      let shard: Shard;
      if !LOAD_FROM_FILE {
        println!("#{}: Creating new DB...", i);
        let db_eles = generate_db_eles(m);
        shard = Shard::from_base64_strings(&db_eles, lwe_dim, m, ele_size, plaintext_bits);
      } else {
        println!("#{}: Loading DB from file...", i);
        shard = Shard::from_json_file(&db_path, lwe_dim, m, ele_size, plaintext_bits);
      }
      if WRITE_TO_FILE {
        println!("#{}: Writing shard to file...", i);
        shard.write_to_file(&db_path, &params_path);
      }
      // derive LHS for improving runtimes
      shard.get_params().derive_lhs();
      println!("#{}: DB & param loading complete, time: {:?}", i, db_start.elapsed());
      tx1.send((shard, i)).unwrap();
    });
  }

  // create queries
  drop(tx1);
  let mut queries = vec![None; num_shards];
  let mut shards_wrapped = vec![None; num_shards];
  while let Ok((shard, i)) = rx1.recv() {
    let quer_start = Instant::now();
    println!("#{}: Creating queries...", i);
    let mut individual_queries = Vec::with_capacity(QUERY_ROW_INDEX);
    for j in 0..QUERY_ROW_INDEX {
      let mut qp = QueryParams::new(&mut shard.get_params());
      qp.prepare(j);
      individual_queries.push(qp);
    }
    queries[i] = Some(individual_queries);
    shards_wrapped[i] = Some(shard);
    println!("#{}: query setup: {:?}", i, quer_start.elapsed());
  }
  let shards: Vec<Shard> = shards_wrapped.into_iter().map(|x| x.unwrap()).collect();

  // send broadcast and receive
  println!("Starting shards");
  let mut bus: Bus<(usize,Vec<String>)> = Bus::new(100);
  let (tx2, rx2) = mpsc::sync_channel(num_shards);
  for i in 0..num_shards {
    let mut rx = bus.add_rx();
    let tx2 = tx2.clone();
    let shard = shards[i].clone();
    thread::spawn(move || {
      let mut db = shard.get_db();
      db.switch_fmt();
      // loop while queries are being received
      while let Ok(recv) = rx.recv() {
        let resp_start = Instant::now();
        // get correct query portion
        println!("#{}.{}: received query", i, recv.0);
        let serialized_query = recv.1[i].clone();
        let res = shard.respond(&serialized_query);
        tx2.send((i, recv.0, res, db.get_row(recv.0))).unwrap();
        println!("#{}.{}: response computed: {:?}", i, recv.0, resp_start.elapsed());
      }
    });
  }
  
  // broadcast serialized queries
  let combined_queries: Vec<Vec<QueryParams>> = (0..QUERY_ROW_INDEX).into_iter().map(|j| queries.iter().map(|collections| collections.as_ref().unwrap()[j].clone()).collect()).collect();
  println!("Sending queries");
  let broadcast_start = Instant::now();
  for i in 0..QUERY_ROW_INDEX {
    let serialized = combined_queries[i].iter().map(|qp| qp.get_query().serialize()).collect();
    bus.broadcast((i, serialized));
  }
  // drop things
  drop(tx2);
  drop(bus);
  while let Ok(res) = rx2.recv() {
    let qp = &combined_queries[res.1][res.0];
    let resp = Response::deserialize(&res.2);
    let output_row = resp.parse_output_as_row(qp);
    let output = resp.parse_output_as_base64(qp);
    if res.3 != output_row {
      panic!("#{}.{}: *** Failed *** expected={:?} actual={:?}", res.0, res.1, res.3, output_row);
    } else if DEBUG {
      println!("#{}.{}: Success! output={}", res.0, res.1, output);
    }
  }
  println!("All queries complete, time: {:?}", broadcast_start.elapsed());
}

fn generate_db_eles(total_num_eles: usize) -> Vec<String> {
  let mut eles = Vec::with_capacity(total_num_eles);
  for _ in 0..total_num_eles {
    let mut ele = vec![0u8; ELE_BYTE_LEN];
    OsRng.fill_bytes(&mut ele);
    eles.push(base64::encode(ele));
  }
  eles
}
