use pi_rs_cli_utils::*;
use criterion::{criterion_group, criterion_main, BenchmarkGroup, Criterion};
use pi_rs::api::{QueryParams,Response,Shard};

const BENCH_ONLINE: bool = true;
const BENCH_DB_GEN: bool = true;

fn criterion_benchmark(c: &mut Criterion) {
  let CLIFlags { m, lwe_dim, ele_size, plaintext_bits, .. } = parse_from_env();
  let mut lwe_group = c.benchmark_group("lwe");
  
  println!("Setting up DB for benchmarking...");
  let db_eles = bench_utils::generate_db_eles(m, (ele_size+7)/8);
  let shard = Shard::from_base64_strings(&db_eles, lwe_dim, m, ele_size, plaintext_bits);
  println!("Setup complete, starting benchmarks");
  if BENCH_ONLINE {
    _bench_client_query(&mut lwe_group, &shard);
  }
  if BENCH_DB_GEN {
    lwe_group.sample_size(10);
    _bench_db_generation(&mut lwe_group, &shard, &db_eles);
  }
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);

fn _bench_db_generation(c: &mut BenchmarkGroup<criterion::measurement::WallTime>, shard: &Shard, db_eles: &[String]) {
  let db = shard.get_db();
  let mut params = shard.get_params();
  let w = db.get_matrix_width_self();

  println!("Starting DB generation benchmarks");
  c.bench_function(
    format!("generate db and params, m: {}, w: {}", db.get_matrix_height(), w),
    |b| {
      b.iter(|| {
        Shard::from_base64_strings(db_eles, params.get_dim(), db.get_matrix_height(), db.get_ele_size(), db.get_plaintext_bits());
      });
    }
  );

  c.bench_function(format!("derive LHS from seed, lwe_dim: {}, m: {}, w: {}", params.get_dim(), db.get_matrix_height(), w), |b| {
    b.iter(|| {
      params.clear_lhs();
      params.derive_lhs();
    });
  });
}

fn _bench_client_query(c: &mut BenchmarkGroup<criterion::measurement::WallTime>, shard: &Shard) {
  let db = shard.get_db();
  let mut params = shard.get_params();
  let w = db.get_matrix_width_self();
  let idx = 10;
  
  println!("Starting client query benchmarks");
  let mut _qp = QueryParams::new(&mut params);
  _qp.prepare(idx);
  let _q = _qp.get_query();
  let _sq = _q.serialize();
  let mut _resp = shard.respond(&_sq);
  c.bench_function(format!("create client query params, lwe_dim: {}, m: {}, w: {}", params.get_dim(), db.get_matrix_height(), w), |b| {
    b.iter(|| QueryParams::new(&mut params));
  });

  c.bench_function(format!("client query prepare, lwe_dim: {}, m: {}, w: {}", params.get_dim(), db.get_matrix_height(), w), |b| {
    b.iter(|| {
      _qp.prepare(idx);
    });
  });
  
  c.bench_function(format!("server response compute, lwe_dim: {}, m: {}, w: {}", params.get_dim(), db.get_matrix_height(), w), |b| {
    b.iter(|| {
      _resp = shard.respond(&_sq);
    });
  });

  c.bench_function(format!("client parse server response, lwe_dim: {}, m: {}, w: {}", params.get_dim(), db.get_matrix_height(), w), |b| {
    b.iter(|| {
      let _parsed = Response::deserialize(&_resp).parse_output_as_base64(&_qp);
    });
  });
}

mod bench_utils {
  use rand_core::{RngCore, OsRng};
  pub fn generate_db_eles(num_eles: usize, ele_byte_len: usize) -> Vec<String> {
    let mut eles = Vec::with_capacity(num_eles);
    for _ in 0..num_eles {
      let mut ele = vec![0u8; ele_byte_len];
      OsRng.fill_bytes(&mut ele);
      let ele_str = base64::encode(ele);
      eles.push(ele_str);
    }
    eles
  }
}
