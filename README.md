# FrodoPIR implementation and benchmarks

The library implementing the primitives for FrodoPIR can be found at [./pi-rs](./pi-rs). Under the [./pi-rs-example](./pi-rs-example) folder, you can find an example on how to use
the FrodoPIR library.

**Useful helpers**:

- Build FrodoPIR: `$ make build` 

- Run benchmarks: `$ make bench` or `$ make bench-all`

For more examples on how to instantiate FrodoPIR with different params, check [Makefile](./Makefile).


---
```
**Abstract**: Private Information Retrieval (PIR) schemes promise
clients the ability to *privately* request resources from a server
database. Unfortunately, existing schemes are either too inefficient,
too complex, or require supporting unrealistic trust assumptions.

We design FrodoPIR -- a stateful, single-server PIR scheme that incurs
small online overheads and a preprocessing burden that scales
independently of the number of client queries. The FrodoPIR approach is
based heavily on the learning with errors (LWE) problem, and is very
simple to implement, requiring only a few hundred lines of Rust code.
FrodoPIR is highly amenable to parallelization, and allows careful
configuration of all the major security, efficiency, and functionality
parameters, depending on the desired application. In terms of
performance, for a database of 1 million 1KB elements, FrodoPIR requires
<1 second for responding to a client query, has a server response size
blow-up factor of <3.6x, and financial costs are \$1 for answering
100,000 client queries. Overall, we demonstrate that FrodoPIR is a
viable PIR scheme for deployment in large, multi-client systems.
```
