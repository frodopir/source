use std::fs;
use std::io::BufReader;

use rand_core::{RngCore,OsRng};
use serde::{Serialize, Deserialize};
use serde_json::json;

use crate::utils::format::*;
use crate::utils::matrices::*;

#[derive(Clone)]
pub struct Database {
  entries: Vec<Vec<u32>>,
  m: usize,
  ele_size: usize,
  plaintext_bits: usize,
}
impl Database {
  pub fn new(elements: &[String], m: usize, ele_size: usize, plaintext_bits: usize) -> Self {
    Self { entries: swap_matrix_fmt(&construct_rows(elements, m, ele_size, plaintext_bits)), m, ele_size, plaintext_bits }
  }

  pub fn load(db_file: &str, m: usize, ele_size: usize, plaintext_bits: usize) -> Self {
    let file_contents: String = fs::read_to_string(db_file).unwrap().parse().unwrap();
    let elements: Vec<String> = serde_json::from_str(&file_contents).unwrap();
    Self { entries: swap_matrix_fmt(&construct_rows(&elements, m, ele_size, plaintext_bits)), m, ele_size, plaintext_bits }
  }

  pub fn from_file(db_file: &str, m: usize, ele_size: usize, plaintext_bits: usize) -> Self {
    let file_contents: String = fs::read_to_string(db_file).unwrap().parse().unwrap();
    let entries: Vec<Vec<u32>> = serde_json::from_str(&file_contents).unwrap();
    Self { entries: entries, m, ele_size, plaintext_bits }
  }

  pub fn get_index(&self, idx: usize) -> Vec<u32> {
    self.entries[idx].clone()
  }

  pub fn switch_fmt(&mut self) {
    self.entries = swap_matrix_fmt(&self.entries);
  }

  pub fn vec_mult(&self, row: &[u32], col_idx: usize) -> u32 {
    let mut acc = 0u32;
    for i in 0..row.len() {
      acc = acc.wrapping_add(row[i].wrapping_mul(self.entries[col_idx][i]));
    }
    acc
  }

  pub fn write_to_file(&self, path: &str) {
    let json = json!(self.entries);
    serde_json::to_writer(&fs::File::create(path).unwrap(), &json).unwrap();
  }

  /// Returns the ith row of the DB matrix
  pub fn get_row(&self, i: usize) -> Vec<u32> {
    self.entries[i].clone()
  }

  /// Returns the width of the DB matrix
  pub fn get_matrix_width(element_size: usize, plaintext_bits: usize) -> usize {
    let mut quo = element_size / plaintext_bits;
    if element_size % plaintext_bits != 0 {
      quo = quo + 1;
    }
    quo
  }
  
  /// Returns the width of the DB matrix
  pub fn get_matrix_width_self(&self) -> usize {
    Database::get_matrix_width(self.get_ele_size(), self.get_plaintext_bits())
  }

  pub fn get_matrix_height(&self) -> usize {
    self.m
  }
  
  pub fn get_ele_size(&self) -> usize {
    self.ele_size
  }

  pub fn get_plaintext_bits(&self) -> usize {
    self.plaintext_bits
  }
}

/// The `Params` object allows loading and interacting with params that
/// are used by the client for constructing queries
#[derive(Serialize, Deserialize, Clone)]
pub struct Params {
  dim: usize,
  m: usize,
  lhs_seed: [u8; 32],
  lhs: Option<Vec<Vec<u32>>>,
  rhs: Vec<Vec<u32>>,
  ele_size: usize,
  plaintext_bits: usize,
}
impl Params {
  pub fn new(db: &Database, dim: usize) -> Self {
    let lhs_seed = generate_seed();
    Self { 
      lhs_seed: lhs_seed, 
      lhs: None, 
      rhs: Self::generate_params_rhs(db, lhs_seed, dim, db.m), 
      dim, 
      m: db.m, 
      ele_size: db.ele_size, 
      plaintext_bits: db.plaintext_bits}
  }

  /// Load params from a JSON file
  pub fn load(params_path: &str, dim: usize, m: usize) -> Self {
    let reader = BufReader::new(fs::File::open(params_path).unwrap());
    let mut params: Params = serde_json::from_reader(reader).unwrap();
    params.lhs = Some(swap_matrix_fmt(&get_lwe_matrix_from_seed(params.lhs_seed, dim, m)));
    params
  }

  /// Derives the LHS of the params struct
  pub fn derive_lhs(&mut self) {
    if self.lhs.is_none() {
      self.lhs = Some(swap_matrix_fmt(&get_lwe_matrix_from_seed(self.lhs_seed, self.dim, self.m)));
    }
  }

  /// CLears the LHS for situations where it is not required
  pub fn clear_lhs(&mut self) {
    self.lhs = None;
  }
  
  /// Generates the RHS of the params using the database and the seed
  /// for the LHS
  pub fn generate_params_rhs(db: &Database, lhs_seed: [u8; 32], dim: usize, m: usize) -> Vec<Vec<u32>> {
    let lhs = get_lwe_matrix_from_seed(lhs_seed, dim, m);
    (0..Database::get_matrix_width(db.ele_size, db.plaintext_bits)).into_iter().map(|i| {
      let mut col = Vec::with_capacity(m);
      for r in &lhs {
        col.push(db.vec_mult(r, i));
      }
      col
    }).collect()
  }

  /// Writes the params struct as JSON to file
  pub fn write_to_file(&self, path: &str) {
    let json = json!({
      "lhs_seed": self.lhs_seed,
      "rhs": self.rhs,
    });
    serde_json::to_writer(&fs::File::create(path).unwrap(), &json).unwrap();
  }

  /// Computes s*A + e using the seed used to generate the LHS matrix of
  /// the public parameters
  pub fn mult_left(&self, s: &[u32]) -> Vec<u32> {
    let cols = self.lhs.as_ref().unwrap();
    (0..cols.len()).into_iter().map(|i| {
      let s_a = vec_mult_u32_u32(s, &cols[i]);
      let e = random_ternary();
      s_a.wrapping_add(e)
    }).collect()
  }

  /// Computes s*(A*DB) using the RHS of the public parameters
  pub fn mult_right(&self, s: &[u32]) -> Vec<u32> {
    let cols = &self.rhs;
    (0..cols.len()).into_iter().map(|i| {
      vec_mult_u32_u32(&s, &cols[i])
    }).collect()
  }

  pub fn get_dim(&self) -> usize {
    self.dim
  }
  
  pub fn get_ele_size(&self) -> usize {
    self.ele_size
  }

  pub fn get_plaintext_bits(&self) -> usize {
    self.plaintext_bits
  }
}

fn construct_rows(elements: &[String], m: usize, ele_size: usize, plaintext_bits: usize) -> Vec<Vec<u32>> {
  let row_width = Database::get_matrix_width(ele_size, plaintext_bits);
  (0..m).into_iter().map(|i| {
    let mut row = Vec::with_capacity(row_width);
    let data = &elements[i];
    let bytes = base64::decode(&data).unwrap();
    let bits = bytes_to_bits_le(&bytes);
    for i in 0..row_width {
      let end_bound = (i+1)*plaintext_bits;
      if end_bound < bits.len() {
        row.push(bits_to_u32_le(&bits[i*plaintext_bits..end_bound]));
      } else {
        row.push(bits_to_u32_le(&bits[i*plaintext_bits..]));
      }
    }
    row
  }).collect()
}

fn generate_seed() -> [u8; 32] {
  let mut seed = [0u8; 32];
  OsRng.fill_bytes(&mut seed);
  seed
}